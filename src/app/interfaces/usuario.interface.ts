// export interface UsuarioI {
//     usuario: string;
//     password: string;
// }

export interface UsuarioDataI {
    nombres: string;
    apellidos: string;
    email: string;
    celular: number;
    fecha: string;
    hora: string;
    descripcion: string;
}